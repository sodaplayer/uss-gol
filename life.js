(function() {
  'use strict';

  var mouseMove = function mouseMove(e) {
    var mousePos = relPos(e);
    var cell = getCell(mousePos);
    Life.handleMouse(cell, MEvents.MOVE);
  }

  var mouseUp = function mouseUp(e) {
    var mousePos = relPos(e);
    Life.handleMouse(null, MEvents.UP);
  }

  var mouseDown = function mouseDown(e) {
    var mousePos = relPos(e);
    var cell = getCell(mousePos);
    Life.handleMouse(cell, MEvents.DOWN);
  }

  /**
   * Returns the mouse position relative to the canvas.
   *
   * @param <MouseEvent> e
   * @return {x, y}
   */
  var relPos = function relPos(e) {
    var offsetX = 0;
    var offsetY = 0;
    var current = e.target;

    do {
      offsetX += current.offsetLeft;
      offsetY += current.offsetTop ;
    } while (current = current.offsetParent);

    return {
      x: e.pageX - offsetX,
      y: e.pageY - offsetY
    };
  }

  var getCell = function getCell(mousePos) {
    var x = Math.max(0, Math.min(Life.grid.width - 1,
                     Math.floor(mousePos.x / Life.scaleI.value)));
    var y = Math.max(0, Math.min(Life.grid.height - 1, 
                     Math.floor(mousePos.y / Life.scaleI.value)));

    return {
      x: x,
      y: y
    }
  }

  this.Life = {
    init: function(canvas, scaleI, widthI, heightI) {
      this.canvas = canvas;
      this.ctx = canvas.getContext('2d');

      this.scaleI = scaleI;
      this.widthI = widthI;
      this.heightI = heightI;

      this.dragging = false;

      var width = parseInt(widthI.value);
      var height = parseInt(heightI.value);
      var scale = parseInt(scaleI.value);

      canvas.width = scale * width + 1;
      canvas.height = scale * height + 1;

      canvas.addEventListener("mousedown", mouseDown, false);
      window.addEventListener("mouseup", mouseUp, false);
      canvas.addEventListener("mousemove", mouseMove, false);


      this.grid = Object.create(Grid);
      var grid = this.grid;

      grid.init(width, height);
      grid.light(7, 6);
      grid.light(6, 8);
      grid.light(7, 8);
      grid.light(8, 8);
      grid.light(8, 7);

      this.drawGrid();
    },

    resizeGrid: function() {
      this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

      var scale = parseInt(this.scaleI.value);
      var width = parseInt(this.widthI.value);
      var height = parseInt(this.heightI.value);

      this.canvas.width = scale * width + 1;
      this.canvas.height = scale * height + 1;

      this.grid.resize(width, height);

      this.drawGrid();
    },

    drawGrid: function() {
      var ctx = this.ctx;

      ctx.save();

      ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
      ctx.translate(0.5, 0.5);

      ctx.strokeStyle = Styles.grid;
      ctx.fillStyle = Styles.cell;

      var scale = parseInt(this.scaleI.value);

      for (var i = 0; i < this.grid.width; i++) {
        for (var j = 0; j < this.grid.height; j++) {
          if (this.grid.cells[i][j]) {
            this.ctx.fillRect(i * scale, j * scale, scale, scale);
          }

          this.ctx.strokeRect(i * scale, j * scale, scale, scale);
        }
      }

      ctx.restore();
    },

    step: function() {
      this.grid.step();
      this.drawGrid();
    }, 

    handleMouse: function(pos, event) {
      if (event == MEvents.UP) {
        this.dragging = false;
      } else {
        if(event == MEvents.DOWN) {
          this.dragging = true;
          console.log(!this.grid.cells[pos.x][pos.y]);
          this.coloring = !this.grid.cells[pos.x][pos.y];
        }

        if (this.dragging) {

          if (this.coloring) {
            Life.grid.light(pos.x, pos.y);
          } else {
            Life.grid.dim(pos.x, pos.y);
          }
        }
      }

      this.drawGrid();
    },

    play: function() {
      clearInterval(this.playInterval);
      this.playInterval = setInterval(this.step.bind(this), 100);
    },

    stop: function() {
      clearInterval(this.playInterval);
    }
  };

  this.Styles = {
    grid: 'rgb(216,216,216)',
    cell: 'rgb(155,197,221)'
  }

  this.MEvents = {
    UP: "up",
    DOWN: "down",
    MOVE: "move"
  }

  var Grid = {
    /**
     * Creates a grid
     */
    init: function init(width, height) {
      this.width = width,
      this.height = height,

      this.cells = new Array(width);

      for (var i = 0; i < width; i++) {
        this.cells[i] = new Array(height);
      }
    },

    light: function light(x, y) {
      this.cells[x][y] = true;
    },

    dim: function dim(x, y) {
      this.cells[x][y] = false;
    },

    resize: function resize(width, height) {
      var popArray = function(e, i, a) {
        e.pop();
      }

      var pushArray = function(e, i, a) {
        e.push();
      }

      while (this.height > height) {
        this.cells.forEach(popArray); 
        this.height--;
      }

      while (this.height < height) {
        this.cells.forEach(pushArray); 
        this.height++;
      }

      while (this.width > width) {
        this.cells.pop();
        this.width--;
      }
      
      while (this.width < width) {
        this.cells.push(new Array(height));
        this.width++;
      }
    },

    step: function step() {

      var newCells = new Array(this.width);
      for (var n = 0; n < this.width; n++) {
        newCells[n] = new Array(this.height);
      }
      for (var i = 0; i < this.width; i++) {
        for (var j = 0; j < this.height; j++) {
          var count = this.count(i, j);

          newCells[i][j] = this.cells[i][j];

          if (count == 3) {
            newCells[i][j] = true;
          } else if (count < 2 || count > 3) {
            newCells[i][j] = false;
          }
        }
      }

      this.cells = newCells;
    },

    count: function count(x, y) {
      var sum = 0;
      for (var i = x-1; i <= x+1; i++) {
        for (var j = y-1; j <= y+1; j++) {
          if (!(i == x && j == y)) {
            if (this.cells[(i + this.width) % this.width]
                          [(j + this.height) % this.height] === true) {
              sum++;
            }
          }
        }
      }

      return sum;
    }
  };
}).call(this);
